FROM rocker/shiny-verse:4.2.2
#1.7.5.

RUN R -e "install.packages(c('shinydashboard', 'shinyWidgets', 'DT', 'cowplot', 'reshape2', 'ggforce', 'plotly', 'bslib', 'hexbin', 'openxlsx'), repos='http://cran.rstudio.com/')"

RUN wget https://oc.embl.de/index.php/s/GXQ5yvyw89ztmNd/download -O /srv/shiny-server/shiny_object.RData

COPY shiny-server.conf /etc/shiny-server/shiny-server.conf
COPY app.R /srv/shiny-server/
ADD www /srv/shiny-server/www

# make all files readable tmp
RUN chmod -R 755 /srv/shiny-server/

EXPOSE 3838

CMD ["R", "-e", "shiny::runApp('/srv/shiny-server/app.R', host='0.0.0.0', port=3838)"]
